package main

import (
	"document-structure/model"
	"time"
)

func main() {
	var doc model.Document
	doc = model.Document{
		Name:        "Doc #1",
		Description: "Some description",
		CopiesCount: 15,
		StartTime:   time.Date(2023, time.August, 21, 21, 0, 0, 0, time.Local),
		FinishTime:  time.Date(2023, time.August, 30, 21, 0, 0, 0, time.Local),
	}

	doc.PrintDocument()
}
