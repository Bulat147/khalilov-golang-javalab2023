package model

import (
	"fmt"
	"time"
)

type Document struct {
	Name        string
	Description string
	CopiesCount int
	StartTime   time.Time
	FinishTime  time.Time
}

func (document *Document) ChangeDescription(newDescription string) {
	document.Description = newDescription
}

func (document *Document) ChangeCopiesCount(count int) {
	if count < 0 {
		fmt.Println("Count of copies can't be negative number")
		return
	}
	document.CopiesCount = count
}

func (document *Document) PrintDocument() {
	fmt.Printf("{%v, %v, %v, %v, %v}", document.Name, document.Description,
		document.CopiesCount, document.StartTime, document.FinishTime)
}
