package main

import (
	"fmt"
	"interface-ed/array_processor"
)

func main() {
	arr := []int{5, 2, 0, 1}
	processor := array_processor.SumAndZeroProcessor{}

	fmt.Printf("%v \n", processor.ProcessArrayToArray(arr))
	fmt.Printf("%v \n", processor.ProcessArrayToNumber(arr))

	otherArr := []int{-1, 0, 3, -6, 8}
	otherProcessor := array_processor.MultAndNegativeProcessor{}

	resArr, resNum := array_processor.ProcessArray(otherArr, &otherProcessor)
	fmt.Printf("%v, %v", resArr, resNum)
}
