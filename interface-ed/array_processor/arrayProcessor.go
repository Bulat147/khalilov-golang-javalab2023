package array_processor

type ArrayProcessor interface {
	ProcessArrayToArray(arr []int) []int
	ProcessArrayToNumber(arr []int) int
}

type SumAndZeroProcessor struct {
}

func (*SumAndZeroProcessor) ProcessArrayToArray(arr []int) []int {
	newArray := []int{}
	for i := 0; i < len(arr); i++ {
		if arr[i] != 0 {
			newArray = append(newArray, arr[i])
		}
	}
	return newArray
}

func (*SumAndZeroProcessor) ProcessArrayToNumber(arr []int) int {
	summ := 0
	for i := 0; i < len(arr); i++ {
		summ += arr[i]
	}
	return summ
}

type MultAndNegativeProcessor struct {
}

func (*MultAndNegativeProcessor) ProcessArrayToArray(arr []int) []int {
	newArray := []int{}
	for i := 0; i < len(arr); i++ {
		if arr[i] >= 0 {
			newArray = append(newArray, arr[i])
		}
	}
	return newArray
}

func (*MultAndNegativeProcessor) ProcessArrayToNumber(arr []int) int {
	mult := 1
	if len(arr) == 0 {
		return 0
	}
	for i := 0; i < len(arr); i++ {
		mult *= arr[i]
	}
	return mult
}

func ProcessArray(array []int, processor ArrayProcessor) ([]int, int) {
	resultNum := processor.ProcessArrayToNumber(array)
	resultArray := processor.ProcessArrayToArray(array)
	return resultArray, resultNum
}
