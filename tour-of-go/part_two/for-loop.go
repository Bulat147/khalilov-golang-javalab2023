package main

import "fmt"

func main() {
	doForLoopClassic()
	doForLoopOptional()
}

func doForLoopClassic() {
	sum := 0

	// В Go есть только одна конструкция для цикла - это for
	// В Go нет цикла while
	for i := 0; i < 10; i++ {
		sum += i
	}

	fmt.Println(sum)
}

func doForLoopOptional() {
	sum := 1

	// Инициализация и измение переменной в for - необязательны
	// Условие - обязательно (так можно реализовать while)
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}

func doForever() {

	// Так можно написать вечный цикл
	for {
		fmt.Println("Liberty...")
	}
}
