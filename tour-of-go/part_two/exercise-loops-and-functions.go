package main

import (
	"fmt"
	"math"
)

func Sqrt10Loop(x float64) float64 {
	var z float64 = 1.0
	for i := 0; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		fmt.Println(z)
	}
	return z
}

func SqrtWhileChanging(x float64) float64 {
	var z float64 = x / 2
	var iter int = 0
	for {
		dif := (z*z - x) / (2 * z)
		if math.Abs(dif) < 0.0005 {
			break
		}
		z -= dif
		iter += 1
		fmt.Printf("Iteration %v Z %v \n", iter, z)
	}
	return z
}

func main() {
	fmt.Println(SqrtWhileChanging(9000600090300))
}
