package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Print("Go runs on ")
	var helloString = "Hello"

	// В switch тоже можно инициалищировать переменные
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
		// Не нужно прописывать break как в других языках
	case "linux":
		fmt.Println("Linux.")

		// Не обязательно использовать только константы
	case helloString:
		fmt.Println("Why are you hello...")

		// Можно даже результаты выполнения ф-ций подставлять
	case getSomeString():
		fmt.Printf("String from function")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.\n", os)
	}
}

func getSomeString() string {
	return "Dororo"
}
