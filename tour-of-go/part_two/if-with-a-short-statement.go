package main

import (
	"fmt"
	"math"
)

func pow(x, n, lim float64) float64 {

	// В if конструкции, как в for, можно инициализировать переменные
	// Эти переменные видны внутри if, а также в else-ах
	if v := math.Pow(x, n); v < lim {
		return v
	} else if v < 19 {
		return v + 15
	}
	return lim
}

func main() {
	fmt.Println(
		pow(3, 2, 10),
		pow(3, 3, 20),
	)
}
