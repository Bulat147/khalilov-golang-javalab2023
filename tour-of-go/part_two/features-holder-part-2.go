package main

import (
	"fmt"
	"math"
)

func main() {

}

func sqrt(x float64) string {
	if x < 0 {
		return sqrt(-x) + "i"
	}

	// Это ф-ция объединяет несколько переменных в одну строку
	// Дефолтно между значениями ставит пробел
	return fmt.Sprint(math.Sqrt(x))
}
