package main

import (
	"fmt"
	"math"
)

func main() {

	// Экспортированные имена (объекты/классы) начинаются с Большой буквы
	// Неэкспортированные с маленькой
	fmt.Println(math.Pi)

}
