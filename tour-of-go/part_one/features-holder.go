package main

import (
	"fmt"
	"math"
	"math/cmplx"
	"math/rand"
	"time"
	"tour_of_go/impi"
)

var coop, pythi, fortrun bool

// Можно создавать такие блоки объявления переменных
// Причем инициировать только часть
var (
	isDone     bool
	defaultInt int = 56
	naturalInt uint
	complexNum complex128 = cmplx.Sqrt(-11 + 5i)
	flNum      float32    = 5.4
)

func main() {
	fmt.Println("Welcome to the playground!")

	fmt.Println("The time is", time.Now())

	fmt.Println("Random %v", rand.Intn(10))

	fmt.Println("Sqrt %v", math.Sqrt(5))

	fmt.Println(sumNums(5, 11))

	impi.Impi("Just to check packaging \n")

	fmt.Println(coop, pythi, fortrun)

	fmt.Println(isDone, defaultInt, naturalInt, complexNum, flNum)

	// Пример приведения типов
	// В Go только явное приведение типов
	var thost int = 65
	var floty float32 = float32(thost)
	var nature uint32 = uint32(floty)
	fmt.Println(nature)
}

func sumNums(x, y int) int {
	return x + y
}
