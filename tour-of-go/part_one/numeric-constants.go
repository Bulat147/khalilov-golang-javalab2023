package main

import "fmt"

// Можно использовать подобные конструкции констант
const (
	// Это побитовый сдвиг - добавляется 100 нулей
	Big = 1 << 100
	// Убирается 99 цифр справа
	Small = Big >> 99
)

func needInt(x int) int { return x*10 + 1 }
func needFloat(x float64) float64 {
	return x * 0.1
}

func main() {
	fmt.Println(needInt(Small))
	fmt.Println(needFloat(Small))
	fmt.Println(needFloat(Big))
	fmt.Printf("%T", Small)
}
