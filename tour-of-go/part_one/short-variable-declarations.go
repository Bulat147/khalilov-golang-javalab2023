package main

import "fmt"

// За пределами ф-ций объявление переменных через := не действует
// За пределами ф-ций нужно использовать ключенвые слова
func main() {
	var i, j int = 1, 2
	k := 3
	c, python, java := true, false, "no!"

	fmt.Println(i, j, k, c, python, java)
}
