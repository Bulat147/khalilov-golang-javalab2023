package main

import "fmt"

// операторы голого возврата нужно использовать только в коротких ф-циях
// в большиз ф-циях они могут испортить читаемость
func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

func main() {
	fmt.Println(split(17))
}
