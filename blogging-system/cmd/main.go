package main

import (
	"blogging-system/internal/app"
	"blogging-system/internal/controllers"
	"blogging-system/internal/repositories"
	"blogging-system/internal/services"
)

func main() {
	likeRepository := repositories.LikeRepositoryFileImpl{Path: "resources/likes.txt"}
	postRepository := repositories.PostRepositoryFileImpl{Path: "resources/posts.txt"}
	userRepository := repositories.UserRepositoryFileImpl{Path: "resources/users.txt"}
	commentRepository := repositories.CommentRepositoryFileImpl{Path: "resources/comments.txt"}

	likeService := services.LikeServiceBaseImpl{LikeRepository: &likeRepository}
	commentService := services.CommentServiceBaseImpl{CommentRepository: &commentRepository}
	userService := services.UserServiceBaseImpl{UserRepository: &userRepository}
	postService := services.PostServiceBaseImpl{CommentServiceBaseImpl: commentService,
		LikeServiceBaseImpl: likeService, PostRepository: &postRepository}

	userController := controllers.UserController{UserService: &userService}
	postController := controllers.PostController{PostService: &postService}

	ui := app.UI{PostController: postController, UserController: userController, User: nil}

	ui.Start()
}
