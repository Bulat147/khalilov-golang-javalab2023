package models

type Post struct {
	Id       uint16
	Name     string
	Text     string
	AuthorId uint16
}
