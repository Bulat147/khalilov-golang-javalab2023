package models

type User struct {
	Id        uint16
	Email     string
	Nickname  string
	Password  string
	Interests [3]string
}
