package models

type Comment struct {
	Id       uint16
	AuthorId uint16
	PostId   uint16
	Text     string
}
