package controllers

import (
	"blogging-system/internal/models"
	"blogging-system/internal/services"
	"blogging-system/internal/util"
	"fmt"
	"strconv"
)

type PostController struct {
	PostService services.PostService
}

func (controller *PostController) CreatePost(userId uint16) {
	var post models.Post
	fmt.Print("-----------СОЗДАНИЕ ПОСТА-----------\n")
	fmt.Print("name: ")
	name := util.ScanConsoleString()
	fmt.Print("text: ")
	text := util.ScanConsoleString()
	for true {
		fmt.Println("Создать пост?")
		fmt.Println("/yes - да")
		fmt.Println("/no - нет, вернуться в главное меню")
		result := util.ScanConsoleString()
		if result == "/no" || name == "" || text == "" {
			fmt.Println("Не удалось создать пост")
			return
		}
		if result == "/yes" {
			post.AuthorId = userId
			post.Text = text
			post.Name = name
			postId := controller.PostService.CreatePost(&post)
			if postId == 0 {
				fmt.Println("Не удалось создать пост")
				return
			}
			fmt.Printf("Создан пост - '%v' \n", post.Name)
		}
	}
}

func (controller *PostController) LikePost(userId uint16) {
	var like *models.Like
	fmt.Print("-----------ЛАЙК-----------\n")
	fmt.Print("postId: ")
	userAnswer := util.ScanConsoleString()
	id, err := strconv.ParseInt(userAnswer, 10, 64)
	if err != nil {
		fmt.Println("Не верно введен postId")
		return
	}
	postId := uint16(id)
	checkPost := controller.PostService.GetPostById(postId)
	if checkPost == nil {
		fmt.Println("Не удалось поставить лайк - нет поста с данным id")
		return
	}
	for true {
		fmt.Println("Поставить лайк?")
		fmt.Println("/yes - да")
		fmt.Println("/no - нет, вернуться в главное меню")
		result := util.ScanConsoleString()
		if result == "/no" {
			fmt.Println("Не удалось поставить лайк")
			return
		}
		if result == "/yes" {
			like.OwnerId = userId
			like.PostId = postId
			id := controller.PostService.AddLikeToPost(like)
			if id == 0 {
				fmt.Println("Не удалось поставить лайк")
				return
			}
			fmt.Printf("Поставлен лайк с id %v на пост с id %v", id, postId)
		}
	}
}

func (controller *PostController) ShowPost() {
	fmt.Print("-----------ПОЛУЧЕНИЕ ПОСТА-----------\n")
	fmt.Print("postId: ")
	userAnswer := util.ScanConsoleString()
	id, err := strconv.ParseInt(userAnswer, 10, 64)
	if err != nil {
		fmt.Println("Неверно введен postId")
		return
	}
	postId := uint16(id)
	post := controller.PostService.GetPostById(postId)
	if post == nil {
		fmt.Println("Не найден пост с данным id")
		return
	}
	fmt.Printf("postId: %v \n", post.Id)
	fmt.Printf("name: %v \n", post.Name)
	fmt.Printf("authorId: %v \n", post.AuthorId)
	fmt.Printf("%v \n", post.Text)
	fmt.Println()
}

func (controller *PostController) CommentPost(userId uint16) {
	var comment *models.Comment
	fmt.Print("-----------СОЗДАНИЕ КОММЕНТАРИЯ-----------\n")
	fmt.Print("postId: ")
	userAnswer := util.ScanConsoleString()
	id, err := strconv.ParseInt(userAnswer, 10, 64)
	if err != nil {
		fmt.Println("Не верно введен postId")
		return
	}
	postId := uint16(id)
	checkPost := controller.PostService.GetPostById(postId)
	if checkPost == nil {
		fmt.Println("Не удалось оставить комментарий - нет поста с данным id")
		return
	}
	fmt.Print("text: ")
	text := util.ScanConsoleString()
	for true {
		fmt.Println("Поставить лайк?")
		fmt.Println("/yes - да")
		fmt.Println("/no - нет, вернуться в главное меню")
		result := util.ScanConsoleString()
		if result == "/no" {
			fmt.Println("Не удалось оставить комментарий")
			return
		}
		if result == "/yes" {
			comment.AuthorId = userId
			comment.Text = text
			comment.PostId = postId
			id := controller.PostService.AddCommentToPost(comment)
			if id == 0 {
				fmt.Println("Не удалось оставить комментарий")
				return
			}
			fmt.Printf("Оставлен комментарий с id %v на пост с id %v", id, postId)
		}
	}
}
