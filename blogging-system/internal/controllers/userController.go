package controllers

import (
	"blogging-system/internal/models"
	"blogging-system/internal/services"
	"blogging-system/internal/util"
	"fmt"
)

type UserController struct {
	UserService services.UserService
}

func (controller *UserController) RegistrationUser() *models.User {
	var user models.User
	fmt.Print("-----------РЕГИСТРАЦИЯ-----------\n")
	fmt.Print("email: ")
	email := util.ScanConsoleString()
	fmt.Print("nickname: ")
	nickname := util.ScanConsoleString()
	fmt.Print("password: ")
	password := util.ScanConsoleString()
	fmt.Print("interest one: ")
	interest1 := util.ScanConsoleString()
	fmt.Print("interest two: ")
	interest2 := util.ScanConsoleString()
	fmt.Print("interest three: ")
	interest3 := util.ScanConsoleString()
	for true {
		fmt.Println("Завершить регистрацию?")
		fmt.Println("/yes - да")
		fmt.Println("/no - нет, вернуться в главное меню")
		result := util.ScanConsoleString()
		if result == "/no" || email == "" || nickname == "" || password == "" {
			fmt.Println("Не удалось зарегестрировать пользователя")
			return nil
		}
		if result == "/yes" {
			user.Email = email
			user.Nickname = nickname
			user.Password = password
			user.Interests = [3]string{interest1, interest2, interest3}
			userId := controller.UserService.RegistrationUser(&user)
			if userId == 0 {
				fmt.Println("Не удалось зарегестрировать пользователя")
				return nil
			}
			fmt.Printf("Пользоваель зарегестрирован под id - %v \n", userId)
			return &user
		}
	}
	return nil
}

func (controller *UserController) LoginUser() *models.User {
	fmt.Print("-----------ВХОД В СИСТЕМУ-----------\n")
	fmt.Print("email: ")
	email := util.ScanConsoleString()
	fmt.Print("password: ")
	password := util.ScanConsoleString()
	user := controller.UserService.LoginUser(email, password)
	if user == nil {
		fmt.Println("Не удалось войти в систему. Возможно, вы некорректно ввели данные")
		return nil
	}
	fmt.Println("Здравствуйте, " + user.Nickname + "!")
	return user
}
