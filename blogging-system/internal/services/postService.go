package services

import (
	"blogging-system/internal/models"
	"blogging-system/internal/repositories"
)

type LikeService interface {
	AddLikeToPost(like *models.Like) uint16
}

type CommentService interface {
	AddCommentToPost(comment *models.Comment) uint16
}

type PostService interface {
	LikeService
	CommentService
	CreatePost(post *models.Post) uint16
	GetPostById(postId uint16) *models.Post
}

type LikeServiceBaseImpl struct {
	LikeRepository repositories.LikeRepository
}

type CommentServiceBaseImpl struct {
	CommentRepository repositories.CommentRepository
}

type PostServiceBaseImpl struct {
	CommentServiceBaseImpl
	LikeServiceBaseImpl
	PostRepository repositories.PostRepository
}

func (postService *PostServiceBaseImpl) CreatePost(post *models.Post) uint16 {
	return postService.PostRepository.Save(post)
}

func (postService *PostServiceBaseImpl) GetPostById(postId uint16) *models.Post {
	return postService.PostRepository.GetById(postId)
}

func (likeService *LikeServiceBaseImpl) AddLikeToPost(like *models.Like) uint16 {
	return likeService.LikeRepository.Save(like)
}

func (commentService *CommentServiceBaseImpl) AddCommentToPost(comment *models.Comment) uint16 {
	return commentService.CommentRepository.Save(comment)
}
