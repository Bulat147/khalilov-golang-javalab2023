package services

import (
	"blogging-system/internal/models"
	"blogging-system/internal/repositories"
)

type UserService interface {
	RegistrationUser(user *models.User) uint16
	LoginUser(email, password string) *models.User
}

type UserServiceBaseImpl struct {
	UserRepository repositories.UserRepository
}

func (userService *UserServiceBaseImpl) RegistrationUser(user *models.User) uint16 {
	return userService.UserRepository.Save(user)
}

func (userService *UserServiceBaseImpl) LoginUser(email, password string) *models.User {
	return userService.UserRepository.FindByEmailAndPassword(email, password)
}
