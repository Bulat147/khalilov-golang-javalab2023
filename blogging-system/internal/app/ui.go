package app

import (
	"blogging-system/internal/controllers"
	"blogging-system/internal/models"
	"blogging-system/internal/util"
	"fmt"
	"os"
)

type UI struct {
	User           *models.User
	UserController controllers.UserController
	PostController controllers.PostController
}

func (ui *UI) Start() {
	for true {
		fmt.Println()
		ui.printMainMenu()
		result := util.ScanConsoleString()
		switch result {
		case "/registration":
			ui.User = ui.UserController.RegistrationUser()
		case "/exit":
			os.Exit(1)
		case "/createPost":
			if ui.User == nil {
				fmt.Println("Вы не вошли в систему")
				continue
			}
			ui.PostController.CreatePost(ui.User.Id)
		case "/like":
			if ui.User == nil {
				fmt.Println("Вы не вошли в систему")
				continue
			}
			ui.PostController.LikePost(ui.User.Id)
		case "/showPost":
			ui.PostController.ShowPost()
		case "/comment":
			if ui.User == nil {
				fmt.Println("Вы не вошли в систему")
				continue
			}
			ui.PostController.CommentPost(ui.User.Id)
		case "/login":
			ui.User = ui.UserController.LoginUser()
		}
	}
}

func (*UI) printMainMenu() {
	fmt.Println("Выберите действие:")
	fmt.Println("/registration - зарегестрироваться")
	fmt.Println("/login - вход в систему")
	fmt.Println("/createPost - создать пост")
	fmt.Println("/like - поставить лайк")
	fmt.Println("/comment - добавить комментарий к посту")
	fmt.Println("/showPost - посмотреть пост")

	//fmt.Printf("/unlike - убрать лайк с поста")

	//fmt.Println("/deleteComment - удалить комментарий")
	//fmt.Println("/getPosts - посмотреть все посты (только название и id)")
	//fmt.Println("/showComments - посмотреть комментарии к посту")

	fmt.Println("/exit - заверишть работу приложения")
}
