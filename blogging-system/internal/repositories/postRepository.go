package repositories

import (
	"blogging-system/internal/models"
	"bufio"
	"os"
	"strconv"
	"strings"
)

type PostRepository interface {
	Save(post *models.Post) uint16
	GetById(postId uint16) *models.Post
}

type PostRepositoryFileImpl struct {
	Path string
}

func (postRepository *PostRepositoryFileImpl) Save(post *models.Post) uint16 {
	var file *os.File
	var err error
	file, err = os.OpenFile(postRepository.Path, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return 0
	}
	fileScanner := bufio.NewScanner(file)
	var lastString string
	for fileScanner.Scan() {
		lastString = fileScanner.Text()
	}
	if lastString != "" {
		id := getIdFromModelString(lastString)
		if id == 0 {
			return 0
		}
		post.Id = id + 1
	} else {
		post.Id = 1
	}
	_, err2 := file.WriteString(strconv.Itoa(int(post.Id)) + "|" + post.Name + "|" + post.Text + "|" +
		strconv.Itoa(int(post.AuthorId)) + "|" + strconv.FormatBool(false) + "\n")
	if err2 != nil {
		return 0
	}
	return post.Id
}

func (postRepository *PostRepositoryFileImpl) GetById(postId uint16) *models.Post {
	file, err := os.OpenFile(postRepository.Path, os.O_RDONLY|os.O_CREATE, 0777)
	if err != nil {
		return nil
	}
	fileScanner := bufio.NewScanner(file)
	var str string
	for fileScanner.Scan() {
		str = fileScanner.Text()
		id := getIdFromModelString(str)
		if id == 0 {
			continue
		}
		if id == postId {
			return postRepository.getPostFromString(str)
		}
	}
	return nil
}

func (*PostRepositoryFileImpl) getPostFromString(str string) *models.Post {
	var post *models.Post
	values := strings.Split(str, "|")
	if values[4] == "true" {
		return nil
	}
	id := getIdFromString(values[0])
	if id == 0 {
		return nil
	}
	post.Id = id
	post.Name = values[1]
	post.Text = values[2]
	post.AuthorId = getIdFromString(values[3])
	return post
}

func getIdFromModelString(str string) uint16 {
	id := getIdFromString(strings.Split(str, "|")[0])
	return id
}

func getIdFromString(str string) uint16 {
	id, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return uint16(id)
}
