package repositories

import (
	"blogging-system/internal/models"
	"bufio"
	"os"
	"strconv"
)

type CommentRepository interface {
	Save(comment *models.Comment) uint16
}

type CommentRepositoryFileImpl struct {
	Path string
}

func (commentRepository *CommentRepositoryFileImpl) Save(comment *models.Comment) uint16 {
	var file *os.File
	var err error
	file, err = os.OpenFile(commentRepository.Path, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return 0
	}
	fileScanner := bufio.NewScanner(file)
	var lastString string
	for fileScanner.Scan() {
		lastString = fileScanner.Text()
	}
	if lastString != "" {
		id := getIdFromModelString(lastString)
		if id == 0 {
			return 0
		}
		comment.Id = id + 1
	} else {
		comment.Id = 1
	}
	_, err2 := file.WriteString(strconv.Itoa(int(comment.Id)) + "|" + strconv.Itoa(int(comment.AuthorId)) + "|" +
		strconv.Itoa(int(comment.PostId)) + "|" + comment.Text + "|" + strconv.FormatBool(false) + "\n")
	if err2 != nil {
		return 0
	}
	return comment.Id
}
