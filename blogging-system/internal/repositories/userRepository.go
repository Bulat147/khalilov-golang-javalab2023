package repositories

import (
	"blogging-system/internal/models"
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type UserRepository interface {
	Save(user *models.User) uint16
	FindByEmailAndPassword(email, password string) *models.User
}

type UserRepositoryFileImpl struct {
	Path string
}

func (userRepository *UserRepositoryFileImpl) Save(user *models.User) uint16 {
	file, err := os.OpenFile(userRepository.Path, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return 0
	}
	fileScanner := *bufio.NewScanner(file)
	var lastString string
	for fileScanner.Scan() {
		lastString = fileScanner.Text()
	}
	if lastString != "" {
		id, err := strconv.ParseInt(strings.Split(lastString, "|")[0], 10, 64)
		if err != nil {
			return 0
		}
		user.Id = uint16(id + 1)
	} else {
		user.Id = 1
	}
	interests := []string{user.Interests[0], user.Interests[1], user.Interests[2]}
	_, err2 := file.WriteString(strconv.Itoa(int(user.Id)) + "|" + user.Email + "|" + user.Nickname + "|" +
		user.Password + "|" + strings.Join(interests, ",") + "|" + strconv.FormatBool(false) + "\n")
	if err2 != nil {
		log.Fatal(err2)
		return 0
	}
	return user.Id
}

func (userRepository *UserRepositoryFileImpl) FindByEmailAndPassword(email, password string) *models.User {
	file, err := os.OpenFile(userRepository.Path, os.O_RDONLY, 0777)
	if err != nil {
		log.Fatal(err)
	}
	fileScanner := *bufio.NewScanner(file)
	var row string
	for fileScanner.Scan() {
		row = fileScanner.Text()
		if userRepository.checkUserRowByEmailAndPassword(row, email, password) {
			return userRepository.userRowMapper(row)
		}
	}
	return nil
}

func (userRepository *UserRepositoryFileImpl) checkUserRowByEmailAndPassword(row, email, password string) bool {
	if row == "" {
		return false
	}
	values := strings.Split(row, "|")
	// Id|Email|Nickname|Password|Interests|Deleted
	if values[1] == email && values[3] == password && values[5] == "false" {
		return true
	}
	return false
}

func (userRepository *UserRepositoryFileImpl) userRowMapper(row string) *models.User {
	var user models.User
	values := strings.Split(row, "|")
	userId, err := strconv.ParseInt(values[0], 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	user.Id = uint16(userId)
	user.Email = values[1]
	user.Nickname = values[2]
	interests := strings.Split(values[4], ",")
	user.Interests = [3]string{interests[0], interests[1], interests[2]}
	return &user
}
