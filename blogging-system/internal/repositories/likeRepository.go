package repositories

import (
	"blogging-system/internal/models"
	"bufio"
	"os"
	"strconv"
)

type LikeRepository interface {
	Save(like *models.Like) uint16
}

type LikeRepositoryFileImpl struct {
	Path string
}

func (likeRepository *LikeRepositoryFileImpl) Save(like *models.Like) uint16 {
	var file *os.File
	var err error
	file, err = os.OpenFile(likeRepository.Path, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return 0
	}
	fileScanner := bufio.NewScanner(file)
	var lastString string
	for fileScanner.Scan() {
		lastString = fileScanner.Text()
	}
	if lastString != "" {
		id := getIdFromModelString(lastString)
		if id == 0 {
			return 0
		}
		like.Id = id + 1
	} else {
		like.Id = 1
	}
	_, err2 := file.WriteString(strconv.Itoa(int(like.Id)) + "|" + strconv.Itoa(int(like.OwnerId)) + "|" +
		strconv.Itoa(int(like.PostId)) + "|" + strconv.FormatBool(false) + "\n")
	if err2 != nil {
		return 0
	}
	return like.Id
}
