package util

import "fmt"

func ScanConsoleString() string {
	var str string
	fmt.Scanf("%s \n", &str)
	return str
}
